process.env.SOCKET_INNER_PORT = Number(process.env.SOCKET_INNER_PORT) || 8099;

// const fs = require('fs');
// const http2 = require('http2');
const axios = require('axios');
const http = require('http');

const Koa = require('koa');
// const File = require('koa-body');
const Ajv = require('ajv');
const cors = require('koa2-cors');
const formidable = require('koa2-formidable');
const { Client } = require('@elastic/elasticsearch');
const socketio = require('socket.io');

const Storage = require('./storage');
const config = require('./config');
const Components = require('./components');
const Socketio = require('./socket');
const SocketioInner = require('./socket_inner');

const logger = require('./middleware/logger');
const locale = require('./middleware/locale');
// const asyncBusyboy = require('./middleware/async-busboy');
const xResponseTime = require('./middleware/x-response-time');
const error = require('./middleware/error');

const Rabbit = require('./storage/rabbit');

const client = new Client({ node: config.server.rest.elastic });

const app = new Koa(),
      storage = new Storage(app.context, { service: 'rest' }),
      socket = new Socketio(app.context);

app.proxy = true;
app.keys = config.server.rest.keys;
app.context.ajv = new Ajv({
  allErrors: true,
  removeAdditional: true
});

// app.context.rabbit = new Rabbit();
// app.context.rabbit.startPublisher();

app.context.elastic = client;

// app.use(File({
//   multipart: true,
//   formidable: {
//     keepExtensions: true
//   }
// }))
app.use(locale); // logger
app.use(logger); // logger
// app.use(asyncBusyboy);
app.use(xResponseTime);
app.use(error);
// app.use(formidable({ multiples: true }));
// app.use(bodyParser());
app.use(
  cors({
    origin: ['*', 'http://localhost:3000', 'http://localhost:8000'],
    exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
    maxAge: 5,
    credentials: true,
    allowMethods: ['GET', 'POST', 'OPTIONS'],
    allowHeaders: ['Content-Type', 'Authorization', 'Accept']
  })
);

const port = config.server.rest.port;

storage
  .done()
  .then(() => {
    new Components(app);
    let server = http.createServer({}, app.callback());
    const context = app.context
    // socket.done(socketio(server));
    // new SocketioInner(context, socketio(process.env.SOCKET_INNER_PORT));
    server.listen(port, () => console.log('/v1.0.4/ REST Server listening on', port));

    // Обрабатываем аварийные ситуации, когда redis не пустой, а воркеры ничего не делают
  })
  .catch(err => {
    console.error('ERR:', err);
    if (err.code === 'ECONNREFUSED') process.exit();
  });

app.on('error', (err, ctx) => {
  console.log('!!!!', err.message, ctx.url, ctx.status, err);
});