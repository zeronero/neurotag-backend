module.exports = {
  server: {
    rest: {
      enableAuth: false,
      keys: [
        'e87a53e5-24c7-4f79-a886-261aeaf74471',
        '3ad252f4-56e0-4596-ba9a-820611921f41',
        '9e12fffe-399c-4714-b55f-b050c0a72391',
        'e9914a43-8329-4b1f-b3bf-c7412c286d91',
        'd3697fe5-238b-4c02-b489-43437b7f0aa1',
        'a351f178-eebe-456d-98fd-a81a2f114c21',
        '7fbbdf1f-7184-4736-8a00-75cd12b0d621'
      ],
      elastic: process.env.ELASTIC_CONNECTION_URL || 'http://localhost:9200',
      // rabbitMq: process.env.RABBITMQ_CONNECTION_URL || 'amqp://ticketeria:5aaabb4a@localhost:5672',
      rabbitMq: process.env.RABBITMQ_CONNECTION_URL || 'amqp://localhost',
      rabbitMqHttp: { 
        url: process.env.RABBITMQ_HTTP_CONNECTION_URL || 'http://localhost:15672',
        username: process.env.RABBITMQ_HTTP_CONNECTION_URL_USERNAME || 'guest',
        password: process.env.RABBITMQ_HTTP_CONNECTION_URL_PASSWORD || 'guest',
      },
      port: process.env.PORT || 4500,
      db: { 
        url: process.env.MONGO_CONNECTION_URL || 'mongodb://127.0.0.1:27017/ticketeria_admin'
      },
      // db2: {
      //   url: process.env.MONGO_CONNECTION_URL_2 || 'mongodb://dev2:wcawef24@167.71.66.159:27017'
      // },
      db2: {
        url: process.env.MONGO_CONNECTION_URL_2 || 'mongodb://127.0.0.1:27017/ticketeria_admin'
      },
      dbLogs: {
        url: process.env.MONGO_CONNECTION_URL_LOGS || 'mongodb://127.0.0.1:27017/ticketeria_Logs'
      },
      redis: {
        port: process.env.REDIS_CONNECTION_PORT || 6379, // Redis port
        host: process.env.REDIS_CONNECTION_URL || '127.0.0.1', // Redis host
        db: 0
      },
      images: {
        server: process.env.IMAGINARY_CONNECTION_URL || 'http://localhost:9000',
        staticPath: process.env.IMAGE_PATH || 'D:/Server/projects/ticketeria.es/dev/static/',
      }
    }
  }
};

// openssl req -x509 -nodes -subj '//CN=localhost' -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365