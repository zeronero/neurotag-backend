// const rc = require('rc');
// const APP_NAME = 'ticketeria';
const os = require('os');

let platform = os.platform();
console.log(platform);
let config;

if (platform === 'win32')
  config = require('./dev');
else
  config = require('./dist');

module.exports = config;