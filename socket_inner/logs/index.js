const SockeServer = require('../base-class');
const Logs = require('./api');

module.exports = class LogsSocket extends SockeServer {
    constructor(ctx) {
        super({
            serviceName: 'SOCKET-PROXY-LOGS',
            namespace: 'logs',
            store: 'logs',
        }, ctx);

        const logs = new Logs(ctx);

        this.logError('Start socket proxy server for import service');

        super.init((socket) => {
            logs.setSocket(socket);
            socket.on('import_streams_error', (data) => logs.serviceListener(data));
            socket.on('gateways_error_log', (data) => logs.gatewaysErrorLog(data));
            socket.on('fields_request', (data) => logs.fieldsRequest(data));
            socket.on('fields_request_wait', (data) => logs.fieldsRequestWait(data));
            socket.on('fields_request_end', (data) => logs.fieldsRequestEnd(data));
            socket.on('groops_request', (data) => logs.groopsRequest(data));
            socket.on('groops_request_wait', (data) => logs.groopsRequestWait(data));
            socket.on('groops_request_end', (data) => logs.groopsRequestEnd(data));
            socket.on('import_streams_progress_log', (data) => logs.streamsProgressLog(data));
            socket.on('import_streams_progress_log_close', (data) => logs.streamsProgressLogClose(data));
            socket.on('import_gateways_progress_log', (data) => logs.gatewaysProgressLog(data));
            socket.on('import_gateways_progress_log_close', (data) => logs.gatewaysProgressLogClose(data));
        })
    }
};