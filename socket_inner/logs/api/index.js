const modulePath = 'utils';
const { Service } = require(modulePath);

module.exports = class Logs extends Service {
  constructor(ctx) {
    super({ serviceName: 'SOCKET-INNER-LOGS-API' });
    this.ctx = ctx;
    this.io = ctx.socketio_inner.logs.io;
  }
  setSocket(socket) {
    this.socket = socket;
  }
  serviceListener(data) {
    // Sending a message to everyone in rest-admin namespace
    this.ctx.socketio.io.emit('import_streams_error', data);
  }
  gatewaysErrorLog(data) {
    // Sending a message to everyone in rest-admin namespace
    this.ctx.socketio.io.emit('gateways_error_log', data);
  }
  streamsProgressLog(data) {
    this.logError('progress stream: ', data);
    this.ctx.socketio.io.emit('import_streams_progress_log', data);
  }
  async streamsProgressLogClose(data) {

    this.ctx.socketio.io.emit('import_streams_progress_log_close', data);
  }
  gatewaysProgressLog(data) {
    this.logError('progress gateway: ', data);
    this.ctx.inprogress_storage.set('gateway', true)
    this.ctx.socketio.io.emit('import_gateways_progress_log', data);
  }
  async gatewaysProgressLogClose(data) {
    try {
      let id = await this.ctx.redis.lpop('gateway_queue')
      let data = await this.ctx.redis.get(`gateway_${id}`)
      this.ctx.rabbit.publish("", "jobs", new Buffer.from(data))
      await this.ctx.redis.del(`gateway_${id}`)
      this.ctx.inprogress_storage.set('gateway', true)
    } catch (e) {
      this.ctx.inprogress_storage.set('gateway', false)
    }

    this.logError('progress gateway end: ', data);
    this.ctx.socketio.io.emit('import_gateways_progress_log_close', data)
  }
  // TODO получить сокет клиента
  fieldsRequest(data) {
    // console.log(data)
    let { socket_context, _ } = data
    if (this.ctx.socketio.connectedPool.has(socket_context)) {
      this.ctx.socketio.connectedPool.get(socket_context).emit('fields_request', data);
    } else {
      this.logError('Storage don\'t contain socket: ', socket_context);
    }
  }
  fieldsRequestWait(data) {
    let { socket_context, _ } = data
    if (this.ctx.socketio.connectedPool.has(socket_context)) {
      this.ctx.socketio.connectedPool.get(socket_context).emit('fields_request_wait');
    } else {
      this.logError('Storage don\'t contain socket: ', socket_context);
    }
  }
  fieldsRequestEnd(data) {
    let { socket_context, complete } = data
    if (this.ctx.socketio.connectedPool.has(socket_context)) {
      this.ctx.socketio.connectedPool.get(socket_context).emit('fields_request_end', complete);
    } else {
      this.logError('Storage don\'t contain socket: ', socket_context);
    }
  }
  groopsRequest(data) {
    // console.log(data)
    let { socket_context, _ } = data
    if (this.ctx.socketio.connectedPool.has(socket_context)) {
      this.ctx.socketio.connectedPool.get(socket_context).emit('groops_request', data);
    } else {
      this.logError('Storage don\'t contain socket: ', socket_context);
    }
  }
  groopsRequestWait(data) {
    let { socket_context, _ } = data
    if (this.ctx.socketio.connectedPool.has(socket_context)) {
      this.ctx.socketio.connectedPool.get(socket_context).emit('groops_request_wait');
    } else {
      this.logError('Storage don\'t contain socket: ', socket_context);
    }
  }
  groopsRequestEnd(data) {
    let { socket_context, complete } = data
    if (this.ctx.socketio.connectedPool.has(socket_context)) {
      this.ctx.socketio.connectedPool.get(socket_context).emit('groops_request_end', complete);
    } else {
      this.logError('Storage don\'t contain socket: ', socket_context);
    }
  }
};