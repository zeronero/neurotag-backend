const Logs = require('./logs');
// const Admin = require('./admin');

class Socketio {
    constructor(ctx, io) {
        this.ctx = ctx;
        this.ctx.socketio_inner = {};
        this.ctx.socketio_inner.io = io;
        
        new Logs(this.ctx);
        // new Admin(this.ctx);
    }
}

module.exports = Socketio;