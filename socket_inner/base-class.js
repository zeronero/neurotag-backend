// const modulePath = (platform === 'win32') ? '../utils' : 'utils';
const modulePath = 'utils';
const { Service } = require(modulePath);

module.exports = class SocketServer extends Service {
    constructor(opt, ctx) {
        super({ serviceName: opt.serviceName });
        this.ctx = ctx;
        this.ctx.socketio_inner[opt.store] = {};
        this.namespace = this.ctx.socketio_inner[opt.store].io = this.ctx.socketio_inner.io.of(`/${opt.namespace}`);
    }

    init(callback) {
        this.namespace.on('connection', (socket) => {
            this.logError('Service connected: ', socket.id);

            socket.on('error', (error) => {
                this.logError(`Service error ${socket.id}:`, error);
                socket.disconnect()
            });

            socket.on('disconnect', (reason) => {
                this.logError(`Service disconected ${socket.id}:`, reason);
            });

            callback(socket);
        });
    }
};