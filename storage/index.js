const { MongoClient } = require('mongodb');
const config = require('./../config');
const Redis = require('ioredis');

// const StreamsModel = require('./models/streams');
const DatasetsModel = require('./models/datasets');

class Storage {
  constructor(ctx, o) {
    this.ctx = ctx;
    let options = Object.assign(
      {
        mongo: true,
        redis: false,
        service: null
      },
      o
    );

    let key = options.service || null;
    if (key === null) {
      throw new Error('options.service is Requred');
    }
    this.options = options;
    this.config = config.server[key];
  }

  async mongoConnect() {
    if (this.ctx.db || !this.options.mongo) return;

    console.log('mongo:', this.config.db.url);
    this.ctx.db = this.mongo = await MongoClient.connect(
      this.config.db.url, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      }
    );
  }

  async redisConnect() {
    return new Promise((resolve, reject) => {
      if (this.ctx.redis || !this.options.redis) return resolve();
      this.ctx.redis = this.redis = new Redis(this.config.redis);
      this.redis.on('error', e => {
        console.log('[REDIS] error:', `${e}`);
        this.redis.disconnect();
        reject(e);
      });
      this.redis.on('connect', () => {
        console.log('[REDIS]', `connected to ${this.config.redis.host}:${this.config.redis.port}`);
        resolve()
      });
    });
  }

  async initModels() {
    // this.ctx.db.streams = new StreamsModel(this.ctx, 'streams');
    this.ctx.db.datasets = new DatasetsModel(this.ctx, 'datasets');
  }

  async done() {
    await this.mongoConnect();
    await this.redisConnect();
    await this.initModels();
    return this;
  }
}

module.exports = Storage;