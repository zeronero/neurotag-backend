const { ObjectID } = require('mongodb');
const { modelModifiedCheck, modelModifiedErrorCheck, modelDeletedCheck, modelCreatedCheck } = require('./../../utils');
// const ShortId = require('shortid');

module.exports = class DatasetsModel {
  constructor(ctx, collectionName = 'datasets') {
    this.name = collectionName;
    this.db = ctx.db;
    this.redis = ctx.redis;
    this.locales = ctx.locales;
    this.datasets = this.db.db('touchgen').collection(this.name);
  }

  async fetchItemById(_id) {
    return await this.readOne({ _id: new ObjectID(_id) });
  }

  async fetchItemsByShortId(_shortId) {
    return await this.read({ _shortId });
  }

  async fetchItemsByLang(lang) {
    return await this.read({}, {size: 500});
  }

  async fetchItemByLang(lang) {
    return await this.readOne({ lang: lang });
  }

  async create(item) {
    try {
      const operation = await this.datasets.insertOne(item);
      return modelCreatedCheck(operation);
    } catch (e) {
      return modelModifiedErrorCheck(e);
    }
  }

  async readOne(query = {}) {
    const item = await this.datasets.findOne(query);
    if (!item) return [];
    return item;
  }

  async read(query = {}, filters = {}) {

    if(filters.filter && filters.filter !== 'null') {
      query = {name: new RegExp(`^.*${filters.filter}.*$`, 'i') };
    }

    let cursor = await this.datasets.find(query);
    let size = filters.size || 25;
    let total = await cursor.count();

    if(filters.sortBy) {
      let order = {};
      order[filters.sortBy] = 1;
      cursor.sort(order);
    }

    if(filters.page)
      cursor.skip(filters.page * size);
    else
      cursor.skip(0);

    cursor.limit(size);

    let items = await cursor.toArray();

    if (!items) return [];
    return {
      items,
      total
    };
  }

  async update(_id, data) {
    const query = { _id: new ObjectID(_id) },
      modifier = { $set: data },
      options = {
        returnNewDocument: false,
        returnOriginal: false
      };

    try {
      const operation = await this.datasets.findOneAndUpdate(
        query,
        modifier,
        options
      );
      return modelModifiedCheck(operation);
    } catch (e) {
      return modelModifiedErrorCheck(e);
    }
  }

  async delete(_id) {
    const query = { _id: ObjectID(_id) };
    const operation = await this.datasets.deleteMany(query);
    return modelDeletedCheck(operation);
  }
};