// import amqp from 'amqplib/callback_api';
const amqp = require('amqplib');
const config = require('./../config');

// const RABBITMQ_SERVER = 'amqp://localhost';
const RABBITMQ_SERVER = config.server.rest.rabbitMq;

class RabbitMQ {
  constructor() {
    this.connection = null;
    this.pubChannel = null;
    this.workChannel = null;
    this.prefetchCount = 1;
    this.offlinePubQueue = [];
  }

  async connect() {
    return new Promise(async (resolve, reject) => {
      try {
        if(this.connection === null) {
          console.log(`[AMQP] start connecting to: ${RABBITMQ_SERVER}?heartbeat=60`);
          this.connection = await amqp.connect(`${RABBITMQ_SERVER}?heartbeat=60`);
        } else {
          return resolve();
        }
      } catch(e) {
        // todo сделать реконнект
        // todo тут бросать ошибку уровнем выше обрабатывать и делать реконнект
        console.error("[AMQP]", e.message);
        return reject(e);
      }

      this.connection.on("error", (err) => {
        if (err.message !== "Connection closing") {
          console.error("[AMQP] conn error", err.message);
        }

        throw new Error(e);
      });

      this.connection.on("close", () => {
        console.error("[AMQP] reconnecting");
        throw new Error(e);
      });
      console.log("[AMQP] connected");

      process.nextTick(() => {
        resolve();
      });
    });
  }

  closeOnErr(err) {
    if (!err) return false;
    console.error("[AMQP] error", err);
    this.connection.close();
    return true;
  }

  publish(exchange, routingKey, content) {
    try {
      this.pubChannel.publish(
        exchange, 
        routingKey, 
        content, 
        {
          persistent: true,
        },
        (err, ok) => {
          if (err) {
            console.error("[AMQP] publish", err);
            this.offlinePubQueue.push([exchange, routingKey, content]);
            this.pubChannel.connection.close();
          }
        });
    } catch (e) {
      console.error("[AMQP] publish", e.message);
      this.offlinePubQueue.push([exchange, routingKey, content]);
    }
  }  

  async _startPublisher() {
    return new Promise(async (resolve, reject) => {
      try {
        this.pubChannel = await this.connection.createConfirmChannel();
        this.pubChannel.on("error", function (err) {
          console.error("[AMQP] channel error", err.message);
        });
        this.pubChannel.on("close", function () {
          console.log("[AMQP] channel closed");
        });

        while (true) {
          let m = this.offlinePubQueue.shift();
          if (!m) break;
          this.publish(m[0], m[1], m[2]);
        }
        resolve();
      } catch (err) {
        console.log(err);
        // if (this.closeOnErr(err)) throw err;
        reject(err);
      }
    });
  }

  processMsg(msg) {
    this.work(msg, (ok) => {
      try {
        if (ok)
          this.workChannel.ack(msg);
        else
          this.workChannel.reject(msg, true);
      } catch (e) {
        this.closeOnErr(e);
      }
    });
  }
  
  async _startWorker() {
    return new Promise(async (resolve, reject) => {
      try {
        this.workChannel = await this.connection.createChannel();
        this.workChannel.on("error", (err) => {
          console.error("[AMQP] channel error", err.message);
        });
  
        this.workChannel.on("close", () => {
          console.log("[AMQP] channel closed");
        });

        await this.workChannel.prefetch(this.prefetchCount);
        let _ok = await this.workChannel.assertQueue("jobs", { durable: true });
        await this.workChannel.consume("jobs", (msg) => { this.processMsg(msg) }, { noAck: false });
        console.log("Worker is started", _ok);
        resolve();
      } catch(err) {
        console.error(err);
        reject(err);
      }
    });
  }

  work(msg, cb) {
    console.log("Got msg ", msg.content.toString());
    cb(true);
  }

  async startPublisher() {
    try {
      await this.connect();
      await this._startPublisher();
      // setInterval(() => {
      //   console.log('send job');
      //   this.publish("", "jobs", new Buffer.from("work work work"));
      // }, 5000);

    } catch(e) {
      setTimeout(() => {
        console.log('[AMPQ] Restarting:', e);
        this.start();
      }, 5000);
    }
  }

  async startWorker() {
    try {
      await this.connect();
      await this._startWorker();
    } catch(e) {
      setTimeout(() => {
        this.start();
      }, 5000);
    }
  }

  async startTest() {
    try {
      await this.connect();
      await this._startPublisher();
      await this._startWorker();
      setInterval(() => {
        this.publish("", "jobs", new Buffer.from("work work work"));
      }, 5000);

    } catch(e) {
      setTimeout(() => {
        this.start();
      }, 5000);
    }
  }
}

module.exports = RabbitMQ;

// let rmq = new RabbitMQ();
// // rmq.startTest();
// // rmq.startPublisher();
// rmq.work = (msg, cb) => {
//   console.log("!!Got msg ", msg.content.toString());
//   cb(true);
// }

// rmq.startWorker();



// export default RabbitMQ;

