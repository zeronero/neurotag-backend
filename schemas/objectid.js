module.exports = () => {
  return {
    "properties": {
      "_id": {
        "type": "string",
        "pattern": "^[a-f0-9]{24}$"
      }
    },
    "required": [ "_id" ],
    "additionalProperties": false
  };
};
