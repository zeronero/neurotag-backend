class MappingClass {
  constructor() {
    this.jsonDelimery = '.'
  }

  static isNull(item) {
    return item === null
  }

  static notNullAndUndefined(item) {
    return item !== null && item !== undefined
  }

  static isString(item = null) {
    return typeof item === 'string' || item instanceof String
  }

  static isArray(item = null) {
    return Array.isArray(item)
  }

  static isObject(item = null) {
    return (typeof item === 'object' && item !== null) || typeof item === 'function'
  }

  static isBoolean(item) {
    return typeof item === 'boolean'
  }

  isParseDeep(item) {
    if (MappingClass.isString(item)) {
      return false
    }
    if (MappingClass.isNull(item)) {
      return false
    }
    if (MappingClass.isBoolean(item)) {
      return false
    }

    return true
  }

  // получает на вход json
  // возвращает моссив путей до всех значений в json
  flatJson(item = {}, result = [], treeRoot = null) {
    for (const key in item) {
      let currentJsonPath = key
      if (treeRoot !== null) {
        currentJsonPath = `${treeRoot}${this.jsonDelimery}${key}`
      }

      result.push(currentJsonPath)
      if (this.isParseDeep(item[key])) {
        this.flatJson(item[key], result, currentJsonPath)
      }
    }
    return result
  }

  // получает на вход путь в json формата 'performers.performer.0.role_name'
  // возвращает либо значение либо null
  jsonPath(path = null, data) {
    if (data === null || data === undefined) {
      return null
    }
    if (data === '') {
      return null
    }
    if (path === null) {
      return null
    }
    if (!Array.isArray(path)) {
      path = path.split(this.jsonDelimery)
      return this.jsonPath(path, data)
    }

    const currentPath = path.shift()
    if (currentPath === undefined) {
      return data
    }
    if (data[currentPath] === undefined) {
      return null
    }

    return this.jsonPath(path, data[currentPath])
  }

  setJsonPath(path = null, currentData = {}, value = null, sourceData = {}) {
    if (path === null) {
      return sourceData
    }
    if (!Array.isArray(path)) {
      path = path.split(this.jsonDelimery)
      return this.setJsonPath(path, currentData, value, sourceData)
    }

    const currentPath = path.shift()
    if (currentPath === undefined) {
      return sourceData
    }
    if (currentData[currentPath] === undefined && path.length !== 0) {
      currentData[currentPath] = {}
    }

    if (path.length === 0) {
      currentData[currentPath] = value
      return sourceData
    }

    return this.setJsonPath(path, currentData[currentPath], value, sourceData)
  }

  // получает на вход json и путь до массива
  // вернет массив JSONoв где вместо изначального массива по jsonPath будет значения из этого массива
  unwind(data = {}, jsonPath = null) {
    if (jsonPath === null) {
      return [data]
    }
    const items = this.jsonPath(jsonPath, data)
    if (items === null || !Array.isArray(items)) {
      return [data]
    }

    return items.map((item) => {
      let clone = JSON.parse(JSON.stringify(data))
      clone = this.setJsonPath(jsonPath, clone, item, clone)
      return clone
    })
  }

  mapData(item = {}, fields = []) {
    const result = {}
    fields.forEach((field) => {
      const {
        destinationPath,
        sourcePath,
        value
      } = field
      if (destinationPath === undefined) {
        return
      }
      if (value !== undefined) {
        this.setJsonPath(destinationPath, result, value, result)
        return
      }

      const sourceValue = this.jsonPath(sourcePath, item)
      this.setJsonPath(destinationPath, result, sourceValue, result)
    })
    return result
  }
}

class DataTransformerClass {
  constructor(fields, filters) {
    if (!Array.isArray(fields)) {
      throw new TypeError('CONSTRUCTOR ERROR: Fields is not array')
    }

    if (!Array.isArray(filters)) {
      throw new TypeError('CONSTRUCTOR ERROR: Filters is not array')
    }

    const Cmapping = new MappingClass()

    this.mapping = Cmapping
    this.notNullAndUndefined = MappingClass.notNullAndUndefined

    // матричное представление графа взаимосвязи источников
    this.graphMatrix = []

    // массив последовательности генерации данных
    this.calculusArray = []

    // соответсвие имя индекс
    const nameIndexMap = {}
    const fieldsLength = fields.length
    fields.forEach((val, i) => {
      nameIndexMap[val] = i
    })
    filters.forEach((val, i) => {
      nameIndexMap[val.name] = fieldsLength + i
    })

    // генерируем матрицу представления графа взаимосвязи источников
    const length = fieldsLength + filters.length
    for (let i = 0; i < length; i++) {
      const arr = []
      for (let n = 0; n < length; n++) {
        arr.push(0)
      }
      this.graphMatrix.push(arr)
    }

    filters.forEach((val) => {
      const index = nameIndexMap[val.name] // находим индекс фильтра в матрице
      const matrix = this.graphMatrix
      switch (val.type) {
        case 'custom': {
          const arr = matrix[index]
          /*
                                  кастомный фильтр зависит от нескольких источников, находим индекс каждого и ставим единицу,
                                  это значит что несколько источников поступают в фильтр
                                  B   C
                                   \ /
                                    А
                              */
          val.fields.forEach((v) => {
            arr[nameIndexMap[v.sourcePath]] = 1
          })
          break
        }
        default: {
          /*
                                если фильтр зависит от источника, то находим индекс источника и ставим единицу
                                это значит, что данные из этого источника поступают в фильтр
                                B
                                 \
                                  А
                              */
          matrix[index][nameIndexMap[val.sourcePath]] = 1
          break
        }
      }
    })

    // console.info(nameIndexMap)
    // console.info(this.graphMatrix)

    /*
                  A B C D E F  F
                A 0 0 0 0 0 0  |      A = 0
                B 1 0 0 0 0 0  C   A  B = 1 + 0 = 1
                C 0 0 0 0 0 1  |   |  C = 1 + 0 = 1
                D 0 0 1 0 0 0  D   B  D = 1 + 1 + 0 = 2
                E 0 1 0 1 0 0   \ /   E = max(1 + 1 + 0, 1 + 1 + 1 + 0) = 3
                F 0 0 0 0 0 0    E    F = 0
            */

    this.calculusArray = filters
      .map(val => ({
        weight: this._weightCalc(nameIndexMap[val.name]),
        data: val
      }))

    this.calculusArray = this.calculusArray.sort((a, b) => a.weight - b.weight)
  }

  transform(data) {
    if (typeof data !== 'object' || Array.isArray(data)) {
      console.error('TRANSFORM ERROR: data is not object')
    }

    this.calculusArray.forEach((val) => {
      data[val.data.name] = this._transform(data, val.data)
    })

    return data
  }

  _transform(data, filter) {
    switch (filter.type) {
      case 'lowercase': {
        return this._lowercase(this.mapping.jsonPath(filter.sourcePath, data))
      }
      case 'uppercase': {
        return this._uppercase(this.mapping.jsonPath(filter.sourcePath, data))
      }
      case 'sentence_case': {
        return this._sentencecase(this.mapping.jsonPath(filter.sourcePath, data))
      }
      case 'find_replace': {
        return this._findReplace(this.mapping.jsonPath(filter.sourcePath, data), filter.find, filter.replace)
      }
      case 'rounding': {
        return this._rounding(this.mapping.jsonPath(filter.sourcePath, data), filter.number)
      }
      case 'custom': {
        const textArr = filter.fields.map(val => this.mapping.jsonPath(val.sourcePath, data))
        const regArr = filter.fields.map(val => val.find)
        return this._custom(...textArr, ...regArr, filter.result, filter.prop)
      }
    }
  }

  _weightCalc(index) {
    const childWeght = []
    this.graphMatrix[index].forEach((val, i) => {
      if (val === 1) {
        childWeght.push(1 + this._weightCalc(i))
      }
    })
    return !childWeght.length ? 0 : Math.max(...childWeght)
  }

  _uppercase(text) {
    if (!this.notNullAndUndefined(text)) {
      return ''
    }
    return String(text).toUpperCase()
  }

  _lowercase(text) {
    if (!this.notNullAndUndefined(text)) {
      return ''
    }
    return String(text).toLowerCase()
  }

  _sentencecase(text) {
    if (!this.notNullAndUndefined(text)) {
      return ''
    }
    const temp = String(text).toLowerCase()
    return temp[0].toUpperCase() + temp.slice(1)
  }

  _findReplace(text, find, replace) {
    if (!this.notNullAndUndefined(text)) {
      return ''
    }
    return String(text).replace(RegExp(find), replace)
  }

  _rounding(text, num) {
    if (!this.notNullAndUndefined(text)) {
      return ''
    }
    return Number(text).toFixed(Number(num))
  }

  _custom(text1, text2, reg1, reg2, result, prop) {
    let flag
    if (prop.includes('required-A') && !this.notNullAndUndefined(text1)) {
      return ''
    }
    if (prop.includes('required-B') && !this.notNullAndUndefined(text2)) {
      return ''
    }
    if (prop.includes('register')) {
      flag = 'i'
    }
    text1 = String(text1).match(RegExp(reg1, flag))
    text2 = String(text2).match(RegExp(reg2, flag))
    return result
      .replace('{{A}}', text1 === null ? '' : text1[0])
      .replace('{{B}}', text2 === null ? '' : text2[0])
  }
}

const fields = ['_id', 'data_feed_id', 'merchant_id', 'merchant_name', 'aw_product_id', 'aw_deep_link', 'aw_image_url', 'aw_thumb_url', 'category_id', 'category_name', 'brand_id', 'brand_name', 'merchant_product_id', 'merchant_category', 'product_name', 'description', 'promotional_text', 'merchant_deep_link', 'merchant_image_url', 'currency', 'search_price', 'rrp_price', 'delivery_cost', 'custom_1']
const filters = [{
  id: 'Ck9olitUUMw_nJFZhsLa4',
  name: '123 6 10',
  type: 'uppercase',
  sourcePath: '123'
}, {
  id: '032pJDcQ_IeWLZLmMUfV5',
  name: '123',
  type: 'lowercase',
  sourcePath: 'sdf 1'
}, {
  id: 'ixOuQhWhLYO10PoBsrjsu',
  name: 'sdf 1',
  type: 'custom',
  fields: [{
    name: 'A',
    sourcePath: 'currency',
    find: 'G'
  }, {
    name: 'B',
    sourcePath: 'currency',
    find: 'P'
  }],
  prop: ['required-A', 'required-B', 'reset', 'register'],
  result: '{{A}} бла бла бла {{B}}',
  sourcePath: 'currency'
}]
const data = {
  _id: '5e8b0d5c03d2893b88413b47',
  data_feed_id: 14717,
  merchant_id: 7055,
  merchant_name: '365 Tickets',
  aw_product_id: 3622325943,
  aw_deep_link: 'https://www.awin1.com/pclick.php?p=3622325943&a=578041&m=7055',
  aw_image_url: 'https://images2.productserve.com/?w=200&h=200&bg=white&trim=5&t=letterbox&url=ssl%3Acf-r.365ticketsglobal.com%2Fresized%2F486x324%2F39778-ICON_Orlando_Family.jpg&feedId=14717&k=8cf6171aab2390dc2899cc57b82c0a9bcc88bf39',
  aw_thumb_url: 'https://images2.productserve.com/?w=70&h=70&bg=white&trim=5&t=letterbox&url=ssl%3Acf-r.365ticketsglobal.com%2Fresized%2F486x324%2F39778-ICON_Orlando_Family.jpg&feedId=14717&k=8cf6171aab2390dc2899cc57b82c0a9bcc88bf39',
  category_id: 0,
  category_name: '',
  brand_id: 0,
  brand_name: '',
  merchant_product_id: 6864,
  merchant_category: '365Tickets Special Selection',
  product_name: 'Madame Tussauds + SEA LIFE Orlando + ICON Orlando',
  description: "Enjoy 1-Day Admission to three of Orlando's newest Attractions!*  Madame Tussauds Orlando is rolling out the red carpet! Madame Tussauds, the world’s most famous celebrity wax attraction, provides visitors the opportunity to indulge in the ultimate fame experience and signature red carpet treatment.  Shake hands with the president, get on stage with your favorite pop star, or get up close and personal with an A-lister and take the ultimate selfie.  So, who do you want to meet?   SEA LIFE Orlando will transport you into a magical underwater world filled with a dazzling array of amazing creatures! From face-to-face encounters with sharks, to a hands-on touchpool experience, there’s plenty for everyone to enjoy at SEA LIFE Orlando Aquarium, the newest aquarium in Orlando!  To get any closer you’d have to get wet! SEA LIFE is located in the heart of Orlando at I-Drive 360 next to Madame Tussauds Orlando and The Orlando Eye. All three attractions will open May 4th, 2015! Explore the amazing creatures of the Atlantic, Indian and Pacific oceans!        360 Degree Ocean Tunnel      More than 5,000 creatures      Rockpool experience      Interactive “Talking Aquarium” Educational Talks and Feeds      See how they Breed, Rescue and Protect creatures and how the SEA LIFE Trust is working to preserve the oceans for generations to come      ICON Orlando  Take a moment to escape, and see the magic and wonder Orlando has to offer. ICON Orlando offers a whole new way to experience the beauty of the city.  ICON Orlando is bubbling with new experiences! The 400-foot observation wheel takes guests on an aerial journey of Central Florida, offering 360-degree breathtaking views of nearby theme parks and attractions, downtown Orlando and Kennedy Space Center on Florida’s east coast, all within the comfort of 30 air-conditioned glass capsules.   Your experience includes:    A memorable 30 minute experience  Enjoy 360º views from 400 feet in the air  Air conditioned glass capsules  Children ages 3 and under are free  Great for all ages!  Located in the heart of International Drive at ICON Orlando 360    * Visits to other attractions must be used within 30 days of first visit",
  promotional_text: 'A List Party End your visit to Madame Tussauds Orlando with a trip to the Miami strip, as you have been invited to the most exclusive party in town... You can make a dramatic entrance as you step out of your luxury super car and mingle with the stars before entering the club. The party is filled with the most glamorous and gorgeous A-Listers – all waiting to meet you. Ryan Reynolds and Selena Gomez are waiting to chat with you next to a stunning fireplace in the centre of the room. Leonardo DiCaprio and Anne Hathaway are desperate to catch up with you, so you wander over to them by the open archways. Stay for a drink or two and chat with some more of your favorite stars before heading into the courtyard and out into the sunset.  TV Enter via back stage to hear “knock, knock, you’re on stage in 5” – welcome to our TV Zone, a celebration of America’s popular television shows. Your journey takes you into a LIVE TV studio where only the most popular shows are filmed. Be part of Oprah’s show and take a seat, see yourself on the big screen. Have a laugh with Jimmy Fallon before you have your photo taken for the “Modern Family” wall with Sofia Vergara.  Film In our Film Zone we’ll take you on a guided tour through movie making history. Leave your VIP trailer and prepare for your starring role alongside Marilyn Monroe in “Gentlemen Prefer Blondes” before performing martial arts with Jackie Chan. Your singing skills will be put to the test next as you join Olivia Newton-John and John Travolta for some “Grease Lightning” in a scene from the iconic musical “Grease”. Before you know it you’ll be on Eliot’s bike pedalling ET through the night sky in our recreation of the iconic movie scene. Next up you have a breakfast date with Audrey Hepburn, who’s ready and waiting in her famous little black dress from ‘Breakfast at Tiffanys’.  Sports Stars The sounds of stadium crowds can be heard cheering you on as you enter our Sports Zone. Your first stop is in the gym with Muhammad Ali. Grab some boxing gloves and spar against the champ for a fantastic photo opportunity. Your next stop is only a short jog away as you join tennis ace Serena Williams on center court. Our interactive game will tell you how fast you serve a tennis ball. Are you ready to go pro or maybe you would rather just watch the action instead? Take on Tiger Woods on the green, shoot some hoops with Shaq O’Neal or meet soccer star David Beckham. This is the place where you get up close to your sporting heroes!  & many many more!  SEA LIFE Orlando  Explore the Creatures  Are you a Shark lover, Seahorse fanatic or a Clownfish groupie? Perhaps it’s the graceful Jellyfish or the clever Octopus that you love the most. Maybe you simply can’t decide! At SEA LIFE Orlando Aquarium, you can see them all! And you’ll be able to get closer to them than ever before.  Green Sea Turtle Jellyfish Grey Reef Shark Seahorse Cownose Ray Clownfish Tasseled Wobbegong Zebra Shark Barracuda Black Tip Reef Shark Southern Stingray Green Moray Eel Fresh Water Turtle Giant Pacific Octopus Goliath Grouper Sandbar Shark Tarpon Pufferfish Mangrove Stingray  Interactive Rockpool This is where you get (a little) wet! If you’ve ever wondered what lives in the rock pools around our coast, this is the place to find out. Get really close to the wonderful creatures that live on our shores. Everything in our Touchpool is safe to handle ... and our rock pool experts are on hand to show you how!  Things to Do Open all day Touch a Green Sea Anemone - feels sticky and fun! Feel a live starfish Spot the shelled creatures such as a Hermit Crab!  ICON Orlando ICON Orlando is bubbling with new experiences! The 400-foot observation wheel takes guests on an aerial journey of Central Florida, offering 360-degree views of theme parks and attractions, downtown Orlando skyline, and on clear days, Florida’s east coast, all within the comfort of 30 air-conditioned capsules. Select capsules have been transformed to offer the ultimate VIP experience, featuring special décor, an upbeat music selection, selfie sticks, bottle caps seats and complimentary Coca-Cola coolers. Enjoy a refreshing beverage and take in the beautiful views aboard the tallest observation wheel on the North American east coast. At night, the Eye lights up in a rainbow of colors and adds another 64,000 LED lights to the theme park capital of the world. Thus, the attraction is also worth a visit in the dark – not only for those wanting to see the city turn its lights on, but also for photographers wanting to capture the Ferris wheel itself.',
  merchant_deep_link: 'https://www.365tickets.co.uk/i-drive-360/i-drive-360-madame-tussauds-sea-life-orlando-the-orlando-eye',
  merchant_image_url: 'https://cf-r.365ticketsglobal.com/resized/486x324/39778-ICON_Orlando_Family.jpg',
  currency: 'GBP',
  search_price: 0,
  rrp_price: 0,
  delivery_cost: 1.5,
  custom_1: 'North America; United States; Florida; Orlando'
}

const Ctransformer = new DataTransformerClass(fields, filters)

console.info(Ctransformer.transform(data))
