let testObjectId = '5e8aba43256d7c09e3efbba6';
let testData = [
  {
    name: "search_price",
    type: "price",
    value: "free" // бесплатные
  },
  {
    name: "search_price",
    type: "price",
    value: "10" // буквенные значеняи
  },
  {
    name: "search_price",
    type: "price",
    value: "none" // не указана
  },
  {
    name: "search_price",
    type: "price",
    value: [10, 20] // диапазон
  },


  {
    name: "aw_image_url",
    type: "image",
    value: "with" // С изображением
  },
  {
    name: "aw_image_url",
    type: "image",
    value: "without" // Без изображения
  },


  {
    name: "product_name",
    type: "other",
    value: "letter" // буквенные значеняи
  },
  {
    name: "product_name",
    type: "other",
    value: "none" // не указана
  },
  {
    name: "product_name",
    type: "other",
    value: ['Tower', 'London'] // диапазон
  },

  
  {
    name: "product_name",
    type: "unique",
    value: "Tower Bridge Exhibition + London Eye" // точное соответсвие
  }
];

const parsePrice = (item) => {
  let query = {};
  if(item.value === 'free') {
    query.$or = [];
    let a = {};
    let b = {};
    let c = {};
    let d = {};
    let e = {};
    a[item.name] = 0;
    b[item.name] = '';
    c[item.name] = null;
    d[item.name] = {$exists: false};
    e[item.name] = '0';

    query.$or.push(a);
    query.$or.push(b);
    query.$or.push(c);
    query.$or.push(d);
    query.$or.push(e);
    return query;
  }

  if (item.value === 'none') {
    query.$or = [];
    let a = {};
    let b = {};
    let c = {};
    a[item.name] = '';
    b[item.name] = null;
    c[item.name] = {
      $exists: false
    };

    query.$or.push(a);
    query.$or.push(b);
    query.$or.push(c);
    return query;
  }

  if(Array.isArray(item.value)) {
    let [a, b] = item.value;
    query.$and = [];
    let left = {};
    let right = {};
    left[item.name] = {$gte: +a};
    right[item.name] = {$lt: +b};
    query.$and.push(left);
    query.$and.push(right);
    return query;
  }

  query[item.name] = item.value;
  return query;
}

const parseImage = (item) => {
  let query = {};
  if(item.value === 'with') {
    query.$and = [];
    let a = {};
    let b = {};
    let c = {};
    let d = {};

    a[item.name] = {$exists: true};
    b[item.name] = {$ne: null};
    c[item.name] = {$ne: ""};
    d[item.name] = {$type: 2};
    query.$and.push(a);
    query.$and.push(b);
    query.$and.push(c);
    query.$and.push(d);
    return query;
  }

  if(item.value === 'without') {
    query.$or = [];
    let a = {};
    let b = {};
    let c = {};

    a[item.name] = {$exists: false};
    b[item.name] = null;
    c[item.name] = "";
    query.$or.push(a);
    query.$or.push(b);
    query.$or.push(c);
    return query;
  }
  return query;
}

const parseOther = (item) => {
  let query = {};
  if(item.value === 'none') {
    query.$or = [];
    let a = {};
    let b = {};
    let c = {};

    a[item.name] = {
      $exists: false
    };
    b[item.name] = null;
    c[item.name] = "";
    query.$or.push(a);
    query.$or.push(b);
    query.$or.push(c);
    return query;
  }

  if (item.value === 'letter') {
    query.$and = [];
    let a = {};
    let b = {};
    let c = {};
    let d = {};

    a[item.name] = {$exists: true};
    b[item.name] = {$ne: null};
    c[item.name] = {$ne: ""};
    d[item.name] = {$type: 2};
    query.$and.push(a);
    query.$and.push(b);
    query.$and.push(c);
    query.$and.push(d);
    return query;
  }  

  if(Array.isArray(item.value)) {
    let [a, b] = item.value;
    query.$and = [];
    let left = {};
    let right = {};
    left[item.name] = {$gte: +a};
    right[item.name] = {$lt: +b};
    query.$and.push(left);
    query.$and.push(right);
    return query;
  }
  
  query[item.name] = new RegExp(`${item.value}`, 'i');
  return query;
}

const parseUnique = (item) => {
  let query = {};

  query[item.name] = item.value;
  return query;
}

const itemType = (item = null) => {
  let { name, type, value } = item;

  if(type === undefined) return null;
  if(name === undefined) return null;
  if(value === undefined) return null;

  switch(type) {
    case 'price': {
      return parsePrice(item);
      break;
    }

    case 'image': {
      return parseImage(item);
      break;
    }

    case 'other': {
      return parseOther(item);
      break;
    }

    case 'unique': {
      return parseUnique(item);
      break;
    }

    default: 
      return null;
  }

  return null;
}

const requestToQuery = (items = [], type = 'and') => {
//  if(type !== 'or' || type !== 'and') type = 'or';
  let result = {};
  type = `$${type}`;
  result[type] = [];
  items = items.map(item => itemType(item));
  result[type] = items;
  return result;
}

//let x = requestToQuery(testData, 'and');
//console.log(x);


module.exports = requestToQuery;