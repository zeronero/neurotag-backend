const objectIdSchema = require('./../schemas/objectid');
const shortIdSchema  = require('./../schemas/shortId.json');
const langSchema     = require('./../schemas/lang.json');
const langLongSchema = require('./../schemas/langLong.json');
const shortId        = require('./shortId');

const dot2num = (IPv4) => {
  let d = IPv4.split('.');
  return ((((((+d[0]) * 256) + (+d[1])) * 256) + (+d[2])) * 256) + (+d[3]);
}

const validObjectId = async (_id, ctx, next) => {
  let validObjectId = ctx.ajv.validate(objectIdSchema(), { _id });
  if (!validObjectId) {
    return response(ctx, { msg: 'Bad request', data: null }, true, 400);
    // return next();
  }

  return await next();
}

const validShortId = async (_shortId, ctx, next) => {
  let validObjectId = ctx.ajv.validate(shortIdSchema, { _shortId });
  if (!validObjectId) {
    return response(ctx, { msg: 'Bad request', data: null }, true, 400);
    // return next();
  }

  return await next();
}

const seoName = (name) => {
  if (name === null) return name;
  // name = 'Искусство и Театр';
  name = name.normalize('NFD').replace(/[\u0300-\u036f]/g, "").trim();
  name = name.toLowerCase().replace(/[^A-Za-z0-9]/g, ' ').trim().replace(/\s{1,}/g, '-');

  if (name === "") name = "unknown";
  return name;
}


const validLang = async (lang, ctx, next) => {
  let validObjectId = ctx.ajv.validate(langSchema, { lang });
  if (!validObjectId) {
    return response(ctx, { msg: 'Bad request', data: null }, true, 400);
    // return next();
  }

  return await next();
}

const validLangLong = async (lang, ctx, next) => {
  let validObjectId = ctx.ajv.validate(langLongSchema, { lang });
  if (!validObjectId) {
    return response(ctx, { msg: 'Bad request', data: null }, true, 400);
    // return next();
  }

  return await next();
}

const response = async (ctx, data, error = false, status = 200) => {
  data.status = status;
  data.error = error;

  ctx.type = 'application/json';
  ctx.status = status;
  ctx.body = data;
  // console.log('ctx.response', ctx.response);
  ctx.response.remove('Date');
};

const responseSuccess = async (ctx, next, data, total = null) => {
  if(total !== null) return response(ctx, { msg: 'ok', data: data, total });

  response(ctx, { msg: 'ok', data: data });
  return;
  //await next();
}

// const responseError = async (ctx, next, data) => {
//   response(ctx, { msg: 'ok', data: data });
//   await next();
// }

const toNumber = async(ctx, next, fields = []) => {
  fields.forEach(f => {
    if (ctx.request.body[f] !== undefined) {
      ctx.request.body[f] = Number(ctx.request.body[f]);
      if (Number.isNaN(ctx.request.body[f]))
        delete ctx.request.body[f];
    }
  });

  await next();
}

const validBody = async (ctx, next, validationSchema, field = 'body') => {
  let valid = ctx.ajv.validate(validationSchema, ctx.request.body);
  if (!valid) {
    response(ctx, { msg: 'Invalid input', data: ctx.ajv.errors }, true, 405);
    // return await next();
    return;
  }

  ctx.state[field] = ctx.request.body;
  await next();
};

const validDBModifyOperation = async (ctx, next, item) => {
  if (item !== null && item.error) {
    response(
      ctx,
      { msg: item.reason, data: null },
      true,
      item.status
    );
    return await next();
  }
}

const validDBReadByIdOperation = async (ctx, next, item) => {
  if (item === null) {
    response(ctx, { msg: 'not found', data: null }, true, 404);
    return await next();
  }
}

const modelModifiedCheck = (operation) => {
  if (operation.ok === 1) {
    return operation.value;
  }

  return {
    error: true,
    status: 500,
    reason: 'Insert: Internal db Error'
  };
};


const modelCreatedCheck = (operation) => {
//console.log(operation);
  if (operation.result.ok !== 1 || operation.ops.length !== 1) {
    return {
      error: true,
      status: 500,
      reason: 'Insert: Internal db Error'
    };
  }
  return operation.ops[0];
};

const modelModifiedErrorCheck = (e) => {
  if (e.code === 11000) {
    return { error: true, status: 409, reason: 'Allready exists' };
  }

  return { error: true, status: 500, reason: 'Internal Db Error' };
}

const modelDeletedCheck = (operation) => {
  if (operation.result.n !== 1) {
    return { error: true, status: 500, reason: 'Delete: Internal db Error' };
  }
  return { error: false, status: 200, reason: 'ok' };
}

module.exports = {
  validObjectId,
  validBody,
  response,
  responseSuccess,
  validDBReadByIdOperation,
  validDBModifyOperation,
  modelModifiedCheck,
  modelModifiedErrorCheck,
  modelDeletedCheck,
  modelCreatedCheck,
  validShortId,
  validLang,
  validLangLong,
  shortId,
  dot2num,
  toNumber,
  seoName
};