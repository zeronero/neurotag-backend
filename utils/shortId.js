const generate = require('nanoid/generate');
const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

let ShortId = function() {
  return generate(alphabet, 10);
}

module.exports = ShortId;