const { validDBModifyOperation, validDBReadByIdOperation, responseSuccess } = require('./index');

module.exports = class ApiDefault {
  constructor(source = null) {
    if (source === null)
      return new Error('Empty api source field');

    this.source = source;
  }

  async read(ctx, next) {
    console.log('this', this);
    let items = await ctx.db[this.source].read();
    responseSuccess(ctx, next, items);
  }

  async readById(ctx, next) {
    let item = await ctx.db[this.source].fetchItemById(ctx.params._id);
    await validDBReadByIdOperation(ctx, next, item);
    responseSuccess(ctx, next, item);
  }

  async create(ctx, next) {
    let item = await ctx.db[this.source].create(ctx.state[this.source]);
    await validDBModifyOperation(ctx, next, item);
    responseSuccess(ctx, next, item);
  }

  async update(ctx, next) {
    let item = await ctx.db[this.source].update(
      ctx.params._id,
      ctx.state[this.source]
    );
    await validDBModifyOperation(ctx, next, item);
    responseSuccess(ctx, next, item);
  }

  async delete(ctx, next) {
    let deletedItem = await ctx.db[this.source].delete(ctx.params._id);
    await validDBModifyOperation(ctx, next, item);
    responseSuccess(ctx, next, { success: 'ok' });
  }
};
