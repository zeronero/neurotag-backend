module.exports = async (ctx, data, error = false, status = 200) => {
  data.status = status;
  data.error = error;

  ctx.type = 'application/json';
  ctx.status = status;
  ctx.body = data;
};
