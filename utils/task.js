const axios = require('axios');

let ticketmaster = {
  "_id": "5e8aba83256d7c09e3efbba7",
  "name": "Ticketmaster GB",
  "collectionName": "Ticketmaster_GB",
  "dataType": "json",
  "gzip": true,
  "httpMethod": "get",
  "apiUrl": "https://app.ticketmaster.com/discovery-feed/v2/events.json?apikey=AnzY2WA7M49IxSoAD0hcUIQW0U85K0A6",
  "apiParamsCountry": "",
  "apiParamsCity": "",
  "apiParamsPlace": "",
  "apiParamsLanguage": "",
  "apiParamsCurrency": "",
  "apiParamsUrl": "",
  "apiParamsAuthType": "",
  "apiParamsAuthLogin": "",
  "apiParamsAuthPassword": "",
  "apiParamsAuthToken": "",
  "arrayPath": "events",
  "countPath": "",
  "countPathIndex": false,
  "paginatingIndex": false,
  "paginatingOffsetFields": "",
  "paginatingSize": "",
  "paginatingSizeFields": "",
  "paginatingType": false,
  "queryTail": "countryCode",
  "queryTailIndex": true,
  "queryTailValues": "PL,NO",
  "uniqueFields": "eventId"
};

let wegow = {
  "_id": "5e9ecec68fa7930012390f67",
  "name": "wegow",
  "collectionName": "wegow",
  "dataType": "json",
  "gzip": false,
  "httpMethod": "get",
  "apiUrl": "https://www.wegow.com/api/events/?active_tickets=true",
  "apiParamsCountry": "",
  "apiParamsCity": "",
  "apiParamsPlace": "",
  "apiParamsLanguage": "",
  "apiParamsCurrency": "",
  "apiParamsUrl": "",
  "apiParamsAuthType": "",
  "apiParamsAuthLogin": "",
  "apiParamsAuthPassword": "",
  "apiParamsAuthToken": "",
  "arrayPath": "events",
  "countPath": "count",
  "countPathIndex": true,
  "paginatingIndex": true,
  "paginatingOffsetFields": "",
  "paginatingSize": "100",
  "paginatingSizeFields": "page_size",
  "paginatingType": false,
  "queryTail": "",
  "queryTailIndex": false,
  "queryTailValues": "",
  "uniqueFields": "id",
  "paginatingPageFields": "page"
};

let x365Tickets = {
  "_id": "5e8aba43256d7c09e3efbba6",
  "name": "365Tickets",
  "collectionName": "365Tickets",
  "dataType": "csv",
  "gzip": true,
  "httpMethod": "get",
  "apiUrl": "https://productdata.awin.com/datafeed/download/apikey/7ddb09129fb068377dc7920403274c50/fid/14717/format/csv/language/en/delimiter/%2C/compression/gzip/adultcontent/1/columns/data_feed_id%2Cmerchant_id%2Cmerchant_name%2Caw_product_id%2Caw_deep_link%2Caw_image_url%2Caw_thumb_url%2Ccategory_id%2Ccategory_name%2Cbrand_id%2Cbrand_name%2Cmerchant_product_id%2Cmerchant_category%2Cproduct_name%2Cdescription%2Cpromotional_text%2Cmerchant_deep_link%2Cmerchant_image_url%2Ccurrency%2Csearch_price%2Crrp_price%2Cdelivery_cost%2Ccustom_1/",
  "apiParamsCountry": "",
  "apiParamsCity": "",
  "apiParamsPlace": "",
  "apiParamsLanguage": "",
  "apiParamsCurrency": "",
  "apiParamsUrl": "",
  "apiParamsAuthType": "",
  "apiParamsAuthLogin": "",
  "apiParamsAuthPassword": "",
  "apiParamsAuthToken": "",
  "arrayPath": "",
  "countPath": "",
  "countPathIndex": false,
  "paginatingIndex": false,
  "paginatingOffsetFields": "",
  "paginatingSize": "",
  "paginatingSizeFields": "",
  "paginatingType": false,
  "queryTail": "",
  "queryTailIndex": false,
  "queryTailValues": "",
  "uniqueFields": "aw_product_id"
}

let eventful = {
  "_id": "5e8ab9c7256d7c09e3efbba2",
  "name": "Eventful",
  "collectionName": "Eventful",
  "dataType": "json",
  "gzip": false,
  "httpMethod": "get",
  "apiUrl": "http://api.eventful.com/json/events/search?app_key=fnDR6xTRwnn8dL3w&include=categories,subcategories,popularity,tickets,price,links",
  "apiParamsCountry": "",
  "apiParamsCity": "",
  "apiParamsPlace": "",
  "apiParamsLanguage": "",
  "apiParamsCurrency": "",
  "apiParamsUrl": "",
  "apiParamsAuthType": "",
  "apiParamsAuthLogin": "",
  "apiParamsAuthPassword": "",
  "apiParamsAuthToken": "",
  "arrayPath": "events.event",
  "countPath": "total_items",
  "countPathIndex": false,
  "paginatingIndex": true,
  "paginatingOffsetFields": "",
  "paginatingSize": "100",
  "paginatingSizeFields": "pageSize",
  "paginatingPageFields": "page_number",
  "paginatingType": false,
  "queryTail": "location",
  "queryTailIndex": true,
  "queryTailValues": "RU,HU",
  "uniqueFields": "id"
}

let SuiteHop = {
  "_id": "5e8ab9c7256d7c09e3efbba2",
  "name": "SuiteHop",
  "collectionName": "SuiteHop",
  "dataType": "xml",
  "gzip": true,
  "httpMethod": "get",
  "apiUrl": "https://product.impact.com/secure/productservices/catalog/download.irps?p=45x%7B%22networkId%22%3A%221%22%2C%22id%22%3A%22594834%22%2C%22mpId%22%3A%221418014%22%2C%22version%22%3A%22original%22%7DmX6eB1odG%2FiRxw6Kiw9J9g93LkE%3D",
  "apiParamsCountry": "",
  "apiParamsCity": "",
  "apiParamsPlace": "",
  "apiParamsLanguage": "",
  "apiParamsCurrency": "",
  "apiParamsUrl": "",
  "apiParamsAuthType": "",
  "apiParamsAuthLogin": "",
  "apiParamsAuthPassword": "",
  "apiParamsAuthToken": "",
  "arrayPath": "events.event",
  "countPath": "",
  "countPathIndex": false,
  "paginatingIndex": false,
  "paginatingOffsetFields": "",
  "paginatingSize": "",
  "paginatingSizeFields": "",
  "paginatingType": false,
  "queryTail": "",
  "queryTailIndex": false,
  "queryTailValues": "",
  "uniqueFields": "CatalogItemId"
}


class Task {
  constructor(task = null) {
    if(task === null) throw new Error('[Task] Empty Task');
    this.task = this.removeEmptyFields(task);
  }

  async checkUrls(urls) {
    // проверяем первых 5 а лучше вообще 2 сделать 
    // т.к. может быть очень много ссылок в задачах со страницами
    urls = urls.splice(0, 5);
    urls = urls.map(url => this.checkUrl(url));
    urls = await Promise.all(urls);

    return urls;
  }

  async checkUrl(url) {
    return new Promise(async resolve => {
      let api = axios.create({
        timeout: 30000,
      });
  
      try {
        let responce = await api.options(url);
        let { status, statusText, headers } = responce;
        // console.log(responce);
        resolve({ status, statusText, headers, url });
      } catch (e) {
        console.error('Fetch url error:', url, e);
        return resolve({ 
          status: 500,
          statusText: null, 
          headers: null,
          url
        })
      }    
    });
  }

  removeEmptyFields(item = null) {
    if(item === null) return null;
    for (let [key, value] of Object.entries(item)) {
      if(value === "") delete item[key];
    }
    
    return item;
  }

  async fetch(url) {
    let api = axios.create({
      timeout: 30000,
    });

    try {
      let responce = await api.get(url);
      return responce.data;
    } catch (e) {
      console.error('Fetch url error:', url, e);
    }
  }

  getPages(size, total) {
    let result = [];
    if(total <= size) return [1];
    let pageCount = Math.round(total/size);
    for(let i = 0; i < pageCount; i++)
      result.push(i+1);

    // console.log(result);
    return result;
  }

  async getPaginationUrls(urls) {
    urls = urls.map(async url => {
      if (this.task.paginatingIndex !== undefined && this.task.paginatingIndex === true) {
        let page = 1;
        let { paginatingSize, paginatingSizeFields, paginatingPageFields, countPath } = this.task;
        paginatingSize = +paginatingSize;
        if (isNaN(paginatingSize) || paginatingSize < 0)
          paginatingSize = 100;

        const myURL = new URL(url);
        myURL.searchParams.append(paginatingSizeFields, paginatingSize);
        myURL.searchParams.append(paginatingPageFields, page);

        if (countPath === undefined || countPath === null) {
          return [myURL.toString()];
        }

        let pagesData = await this.fetch(myURL.toString())
        if (pagesData === null) {
          return [myURL.toString()];
        }

        if (pagesData[countPath] === undefined) {
          return [myURL.toString()];
        }

        let pages = this.getPages(paginatingSize, pagesData[countPath]);

        pages = pages.map(p => {
          const myURL = new URL(url);
          myURL.searchParams.append(paginatingSizeFields, paginatingSize);
          myURL.searchParams.append(paginatingPageFields, p);
          return myURL.toString();
        });
        // console.log(pages);

        return pages;
      }

      return url;
    });

    urls = await Promise.all(urls);

    return urls;
  }

  getCountriesUrls(urls) {
    if (this.task.queryTailIndex !== undefined && this.task.queryTailIndex === true) {
      let { queryTail, queryTailValues } = this.task;
      if (queryTail === undefined || queryTailValues === undefined) return [];

      queryTailValues = queryTailValues.split(',').map(i => {
        const myURL = new URL(this.task.apiUrl);
        myURL.searchParams.append(queryTail, i);
        return myURL.toString();
      });
      return queryTailValues;
    }

    return urls;
  }

  async getUrl() {
    let urls = [this.task.apiUrl];
    urls = this.getCountriesUrls(urls);
    urls = await this.getPaginationUrls(urls);
    urls = urls.reduce((accumulator, currentValue) => accumulator.concat(currentValue), []);

    return urls;
  }

  getCollectionName() {
    return this.task.collectionName;
  }

  getDataSource() {
    switch (this.task.dataType) {
      case 'csv':
        return this.task.gzip?'gzipCsv':'csv';
        break;
    
      case 'json':
        return this.task.gzip?'gzipJson':'json';
        break;
    
      case 'xml':
        return this.task.gzip?'gzipXml':'xml';
        break;
    
      default:
        return null;
        break;
    }
  }

  getArrayPath() {
    return this.task.arrayPath || null;
  }

  getUniqueField() {
    return this.task.uniqueFields;
  }

  logTask() {
    console.log(JSON.stringify(this.task, true, 2));
  }

  async start() {
    // x.logTask();
    // console.log(x.getCollectionName());
    // console.log(x.getDataSource());
    let urls = await x.getUrl();
    console.log(urls);
    console.log(await x.checkUrls(urls)); 
    // console.log(await x.checkUrl('https://www.wegow.com/api/events/?active_tickets=true&page_size=100&page=1'));
    // console.log(x.getArrayPath());
  }
}

module.exports = Task;

// let x = new Task(ticketmaster);
// let x = new Task(wegow);
// let x = new Task(x365Tickets);
// let x = new Task(eventful);
// let x = new Task(SuiteHop);
// x.start();
// console.log(x.getPages(100, 300));
// // x.parse(wegow);
