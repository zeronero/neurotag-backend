const CampaignName = 'Ticketmaster Spain';
const Amount = 777;
// let status = 'PENDING';
// let status = 'APPROVED';
let status = 'REVERSED';
const ReferringDomain = 'ticketeria.es'

let commonParams = {
  v: "1",
  t: "pageview",
  tid: "UA-138589509-1",
  cid: "fff36eb1-2eae-4b13-84f0-9a27730dace7",
  dh: ReferringDomain,
  pr1nm: "получить из базы по шортайди",
  pr1ca: "получить из базы категорию",
  pr1br: CampaignName,
  pr1pr: "цена из нашей базы",
  pr1qt: "1",
  pr1id: "шортайди"
};

if (status === "PENDING") {
  let step2 = {
    dp: "/step2",
    dt: "Step2",
    pa: "checkout",
    cos: "2"
  };
  
  let pending = {
    dp: "/pending",
    dt: "Pending",
    pa: "purchase",
    ti: "идентификатор транзакции",
    ta: CampaignName,
    tr: Amount
  };

  let sendFirst = Object.assign({}, commonParams, step2);
  let sendSecond = Object.assign({}, commonParams, pending);

  console.log(sendFirst);
  console.log(sendSecond);
}


if (status === 'APPROVED') {
  let approved = Object.assign({}, commonParams, {
    dp: "/approved-step3",
    dt: "Approved Step3",
    pa: "checkout",
    cos: "3",
  })

  console.log(approved);
}

if(status === 'REVERSED') {
  let reversed = {
    v: "1",
    t: "event",
    tid: "UA-138589509-1",
    cid: "1b57a51d-bb79-406a-a84a-cb1ea75fac13",
    ec: "Ecommerce",
    ea: "Refund",
    ni: "1",
    ti: "идентификатор транзакции",
    pa: "refund",
  }

  console.log(reversed);
}



