
// let jsonPath = "attractions.0.attraction.images.1.image.url";
// let jsonPath = "attractions.0.attraction.images.0";
// let jsonPath = "attractions.1";
// let jsonPath = "venue.venueName";
// let jsonPath = "venue";

const regexpMatchArrays = /\.\d{1,}(\.|)/;

const deepArrayGroup = (jsonPath) => {
  let pipeline = [];
  let parts = jsonPath.split(regexpMatchArrays).filter(i => (i !== '.' && i !== ''));
  let arrayIndexs = jsonPath.split('.').filter(p => /\d{1,}/.test(p));
  let last = parts.pop();
  let currentPath = [];
  let matchQuery = {};
  // console.log(last, parts);
  if(parts.length === 0) {
    let $project = { $project: {} };
    $project.$project[last] = 1;
    pipeline.push($project);    
  } else {
    parts.forEach((part, inx) => {
      if (inx === 0) {
        currentPath.push(part);
        let $project = { $project: {} };
        $project.$project[part] = 1;
        pipeline.push($project);
        pipeline.push({
          $unwind: {
            path: `$${part}`,
            includeArrayIndex: `${part}Index`,
            preserveNullAndEmptyArrays: true
          }
        });
  
        matchQuery[`${part}Index`] = +arrayIndexs.pop();
        return;
      }
  
      currentPath.push(part);
      pipeline.push({
        $unwind: {
          path: `$${currentPath.join('.')}`,
          includeArrayIndex: `${part}Index`,
          preserveNullAndEmptyArrays: true
        }
      });
      matchQuery[`${part}Index`] = +arrayIndexs.pop();
    });
  }

  if (arrayIndexs[0] !== undefined) {
    currentPath.push(last);
    pipeline.push({
      $unwind: {
        path: `$${currentPath.join('.')}`,
        includeArrayIndex: `${last}Index`,
        preserveNullAndEmptyArrays: true
      }
    });
    matchQuery[`${last}Index`] = +arrayIndexs.pop();
    last = null;
  }

  pipeline.push({
    $match: matchQuery
  });

  currentPath.push(last);
  currentPath = currentPath.filter(p => p !== null);

  pipeline.push({
    $group: {
      _id: `$${currentPath.join('.')}`
    }
  })
  // console.log(pipeline);
  console.log(last);
  console.log(arrayIndexs);
  console.log(matchQuery);

  return pipeline;
}

const uniquePipeline = (jsonPath) => {
  if (regexpMatchArrays.test(jsonPath)) {
    return deepArrayGroup(jsonPath);
  }

  let pipeline = [];
  let $project = { $project: {} };
  $project.$project[jsonPath] = 1;
  pipeline.push($project);

  pipeline.push({
    $group: {
      _id: `$${jsonPath}`
    }
  })  

  return pipeline;
}

// let pl = uniquePipeline(jsonPath);

module.exports = uniquePipeline;