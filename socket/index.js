// const Redis = require('ioredis');

const Streams = require('./streams');

class Socketio {
    constructor(ctx, io) {
        this.ctx = ctx;
        this.ctx.socketio = {};
        this.ctx.socketio.connectedPool = new Map()
    }

    done(io) {
        this.ctx.socketio.io = io;
        io.on('connection', (socket) => {

            console.log('User connected', socket.id);
            this.ctx.socketio.connectedPool.set(socket.id, socket)
            
            // socket.on('test', data => {
            //     console.info(data);
            //     socket.emit('test', 'hello')
            // });

            socket.on('error', (error) => {
                console.log('User error', socket.id);
                socket.disconnect()
            });

            socket.on('disconnect', function (socket) {
                console.log('User disconnected', socket.id);
            });

            new Streams(this.ctx, socket);

        });
    }

}

module.exports = Socketio;