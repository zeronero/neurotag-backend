const {
    validDBModifyOperation,
    validDBReadByIdOperation,
    responseSuccess,
    seoName
} = require('./../../../utils/');
const Task = require('./../../../utils/task');


module.exports = class Stream {
    constructor(ctx, socket) {
        this.ctx = ctx;
        this.io = ctx.socketio.io;
        this.socket = socket;
    }
    async test (data) {
        this.socket.emit('hi', 'message');
    }
    fieldsRequest (data) {
        data.socket_context = this.socket.id
        this.ctx.socketio_inner.logs.io.emit('fields_request', data)
    }
    fieldsRequestEnd(data) {
        data.socket_context = this.socket.id
        this.ctx.socketio_inner.logs.io.emit('fields_request_end', data)
    }
    groopsRequest(data) {
        console.log('groops_request')
        data.socket_context = this.socket.id
        this.ctx.socketio_inner.logs.io.emit('groops_request', data)
    }
    groopsRequestEnd(data) {
        data.socket_context = this.socket.id
        this.ctx.socketio_inner.logs.io.emit('groops_request_end', data)
    } 
};