const Stream = require('./api');

module.exports = class StreamSocket {
    constructor(ctx, socket) {
        const stream = new Stream(ctx, socket);
        socket.on('stream_test', (data) => stream.test(data));
        socket.on('fields_request', (data) => stream.fieldsRequest(data));
        socket.on('fields_request_end', (data) => stream.fieldsRequestEnd(data));
        socket.on('groops_request', (data) => stream.groopsRequest(data));
        socket.on('groops_request_end', (data) => stream.groopsRequestEnd(data));
    }
};