var http = require('http'),
    express = require('express'),
    Busboy = require('busboy'),
    path = require('path'),
    fs = require('fs');

var app = express();

app.get('/api/datasets', function (req, res) {
    console.log('test')
    res.json({
        error: false,
        data: []
    });
});

app.post('/api/datasets', function (req, res) {
    var busboy = new Busboy({ headers: req.headers });
    busboy
        .on('file', function (fieldname, file, filename, encoding, mimetype) {
            console.log('FILE', fieldname, filename, encoding, mimetype)
            // var saveTo = path.join(__dirname, 'uploads/' + filename);
            // file.pipe(fs.createWriteStream(saveTo))
            file.on('data', function (data) {
                console.log('File [' + fieldname + '] got ' + data.length + ' bytes');
            });
            file.on('end', function () {
                console.log('File [' + fieldname + '] Finished');
            });

            // file.resume()
        })
        .on('close', function (err) {
            console.log("CLOSE")
            console.log(err)
            // res.writeHead(200, { 'Connection': 'close' })
            // res.end("That's all folks!")
        })
        .on('end', function () {
            console.log("END")
            // res.writeHead(200, { 'Connection': 'close' })
            res.end("That's all folks!")
        })
        .on('error', function () {
            console.log("ERROR")
            // res.writeHead(200, { 'Connection': 'close' })
            res.end("That's all folks!")
        })
        .on('finish', function (err) {
            console.log("FINISH")
            console.log(err)
            // res.writeHead(200, { 'Connection': 'close' })
            res.end("That's all folks!")
        })

    return req.pipe(busboy);
});

app.listen(4500);