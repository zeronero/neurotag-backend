FROM node:alpine

WORKDIR /src

COPY package.json /src
RUN apk update && apk add git
RUN npm install
RUN npm audit fix


# Bundle app source
COPY . /src

EXPOSE 4500
CMD [ "npm", "start" ]
