let str = 'fr-CH, fr;q=0.9, en;q=0.8, de;q=0.7, *;q=0.5';

str = str.split(',').map(l => {
  l = l.trim();
  let regExp = /^([a-z]{2}|\*);q=(.*?)$/;
  if (regExp.test(l)) {
    let [input, lang, weight] = l.match(regExp);
    console.log({ lang, weight });
    return { lang, weight };
  } else return null;
}).filter(l => l !== null);

console.log(str);