module.exports = async (ctx, next) => {
  ctx.state['geoIp'] = ctx.state['geoIp'] || {};
  ctx.state['geoIp'].location = null;
  ctx.state['geoIp'].city = ctx.request.headers['x-geoip-city'] || null;
  ctx.state['geoIp'].continent_code = ctx.request.headers['x-geoip-continent_code'] || null;
  ctx.state['geoIp'].country = ctx.request.headers['x-geoip-country'] || null;
  ctx.state['geoIp'].country_code = ctx.request.headers['x-geoip-country_code'] || null;
  ctx.state['geoIp'].latitude = ctx.request.headers['x-geoip-latitude'] || null;
  ctx.state['geoIp'].longitude = ctx.request.headers['x-geoip-longitude'] || null;
  ctx.state['geoIp'].postal_code = ctx.request.headers['x-geoip-postal_code'] || null;
  ctx.state['geoIp'].region = ctx.request.headers['x-geoip-region'] || null;
  ctx.state['geoIp'].region_code = ctx.request.headers['x-geoip-region_code'] || null;
  ctx.state['geoIp'].timezone = ctx.request.headers['x-geoip-timezone'] || null;

  if (ctx.state['geoIp'].latitude !== null) {
    ctx.state['geoIp'].latitude = parseFloat(ctx.state['geoIp'].latitude);
  }

  if (ctx.state['geoIp'].longitude !== null) {
    ctx.state['geoIp'].longitude = parseFloat(ctx.state['geoIp'].longitude);
  }

  if (ctx.request.headers['accept-language'] !== undefined) {
    let str = ctx.request.headers['accept-language'];
    str = str.split(',').map(l => {
      l = l.trim();
      let regExp = /^([a-z]{2}|\*);q=(.*?)$/;
      if (regExp.test(l)) {
        let [input, lang, weight] = l.match(regExp);
        // console.log({ lang, weight });
        return { lang, weight };
      } else return null;
    }).filter(l => l !== null);

    ctx.state['geoIp'].lang = str;
  } else {
    ctx.state['geoIp'].lang = null;
  }

// ctx.request.headers: {
//   city: 'Moscow',
//   continent_code: 'EU',
//   country: 'Russia',
//   country_code: 'RU',
//   latitude: 55.7527,
//   longitude: 37.6172,
//   postal_code: '127490',
//   region: 'Moscow',
//   region_code: 'MOW',
//   timezone: 'Europe/Moscow',
//   lang: null
// }

  let {
    city,
    country_code,
    country,
    latitude,
    longitude
  } = ctx.state['geoIp'];

  if (city === undefined || city === null) return await next();
  if (country_code === undefined || country_code === null) return await next();
  if (country === undefined || country === null) return await next();
  if (latitude === undefined || latitude === null) return await next();
  if (longitude === undefined || longitude === null) return await next();

  ctx.state['geoIp'].location = {
    "name": city,
    "region": null,
    "country": country,
    "code": country_code,
    "location": [
      longitude,
      latitude
    ]
  };

  // console.log('ctx.request.headers:', ctx.request.headers);
  console.log('ctx.request.headers:', ctx.state['geoIp']);
  await next();
};
