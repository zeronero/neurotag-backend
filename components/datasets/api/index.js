const { validDBModifyOperation, validDBReadByIdOperation, responseSuccess, response } = require('./../../../utils/');
// const asyncBusboy = require('async-busboy')
const Busboy = require('busboy')
const nanoid = require('nanoid')
const path = require('path')
const fs = require('fs')
const Minio = require('minio')

const minioClient = new Minio.Client({
  endPoint: 'localhost',
  port: 9000,
  useSSL: false,
  accessKey: 'AKIAIOSFODNN7EXAMPLE',
  secretKey: 'wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY'
});

let bucketExistsIndexDatasets = null
let bucketExistsIndexTransformer = null

module.exports = class Datasets {
  constructor() { }
  async readByShortId(ctx, next) {
    let item = await ctx.db['datasets'].fetchItemsByShortId(ctx.params._shortId);
    await validDBReadByIdOperation(ctx, next, item);
    responseSuccess(ctx, next, item);
  }

  async readById(ctx, next) {
    let item = await ctx.db['datasets'].fetchItemById(ctx.params._id);
    await validDBReadByIdOperation(ctx, next, item);
    responseSuccess(ctx, next, item);
  }

  async readItemByLang(ctx, next) {
    let item = await ctx.db['datasets'].fetchItemByLang(ctx.params._shortId, ctx.params.lang);
    await validDBReadByIdOperation(ctx, next, item);
    responseSuccess(ctx, next, item);
    return;
  }

  async readItemsByLang(ctx, next) {
    let item = await ctx.db['datasets'].fetchItemsByLang(ctx.params.lang);
    await validDBReadByIdOperation(ctx, next, item);
    responseSuccess(ctx, next, item);
    return;
  }

  // filters =
  // { page: '7',
  //   size: '25',
  //   sortBy: 'null',
  //   sortDesc: 'false',
  //   filter: 'test' }
  async read(ctx, next) {
    let query = {};
    let filters = {};
    if(ctx.query !== undefined) {
      if(ctx.query.page) {
        let page = parseInt(ctx.query.page) - 1;

        filters.page = page;

        if(page <= 0 || page >= 1000 || isNaN(page))
          filters.page = 0;
      }

      if(ctx.query.size) {
        let size = parseInt(ctx.query.size);

        filters.size = size;

        if(size <= 0 || size >= 100 || isNaN(size))
          filters.size = 25;
      }

      if(ctx.query.filter) {
        let filter = (ctx.query.filter).toString().trim();

        filters.filter = filter;

        if(filter.length === 0)
          delete filters.filter;
      }

      if(ctx.query.sortBy) {
        let sortBy = (ctx.query.sortBy).toString().trim();

        filters.sortBy = sortBy;

        if(sortBy.length === 0 || sortBy === 'null')
          delete filters.sortBy;
      }

      if(ctx.query.filter) {
        let filter = (ctx.query.filter).toString().trim();

        filters.filter = filter;

        if(filter.length === 0)
          delete filters.filter;
      }


    }
    let items = await ctx.db['datasets'].read(query, filters);

    if(items.total !== undefined)
      responseSuccess(ctx, next, items.items, items.total);
    else
      responseSuccess(ctx, next, items);
  }

  async createMultiLang(ctx, next) {
    let item = await ctx.db['datasets'].createMultiLang(ctx.state['datasets']);
    responseSuccess(ctx, next, item);
  }

  async create(ctx, next) {
    if (bucketExistsIndexDatasets === null) {
      bucketExistsIndexDatasets = await minioClient.bucketExists('dataset')
      if (!bucketExistsIndexDatasets) {
        await minioClient.makeBucket('dataset')
      }
    }

    if (bucketExistsIndexTransformer === null) {
      bucketExistsIndexTransformer = await minioClient.bucketExists('transformer')
      if (!bucketExistsIndexTransformer) {
        await minioClient.makeBucket('transformer')
      }
    }

    let info
    
    try {
      info = await new Promise((resolve, reject) => {
        const info = {}

        const busboy = new Busboy({
          headers: ctx.req.headers,
          limits: {
            files: 2
          }
        });

        const fileStreams = []

        const onFile = async (fieldname, file, filename) => {
          try {
            let name = path.basename(filename)
            const ext = path.extname(name)
            name = name.replace(ext, '')
            name = `${name}-${nanoid()}${ext}`
            switch (fieldname) {
              case 'dataset':
                if (bucketExistsIndexDatasets !== null) {
                  info['dataset'] = name
                  fileStreams.push(new Promise(async (res, rej) => {
                    try {
                      await minioClient.putObject('dataset', name, file)
                      res()
                    } catch (e) {
                      console.error('Error dataset load to S3')
                      rej(e)
                    }
                  }))
                } else {
                  reject(new Error('dataset bucket don\'t exist'))
                }
                break
              case 'transformer':
                if (bucketExistsIndexTransformer !== null) {
                  info['transformer'] = name
                  fileStreams.push(new Promise(async (res, rej) => {
                    try {
                      await minioClient.putObject('transformer', name, file)
                      res()
                    } catch (e) {
                      console.error('Error transform load to S3')
                      rej(e)
                    }
                  }))
                } else {
                  cleanup()
                  reject(new Error('transformer bucket don\'t exist'))
                }
                break
              default:
                cleanup()
                reject(new Error('Undefined field of file'))
                break
            }
          } catch (e) {
            console.error('Unexpected error', e)
            cleanup()
            reject(e)
          }
        }

        const onField = (fieldname, val) => {
          info[fieldname] = val.toString()
        }

        const onGood = async () => {
          if (fileStreams.length > 0) {
            await Promise.all(fileStreams)
          }
          cleanup()
          resolve(info)
        }

        const onError = (err) => {
          console.error("ERROR EVENT", err)
          cleanup()
          reject(err)
        }

        const cleanup = () => {
          busboy.removeListener('file', onFile)
          busboy.removeListener('field', onField)
          busboy.removeListener('close', onError)
          busboy.removeListener('end', onGood)
          busboy.removeListener('error', onError)
          busboy.removeListener('finish', onGood)
          busboy.removeListener('filesLimit', onError)
        }

        busboy
          .on('file', onFile)
          .on('field', onField)
          .on('close', onError)
          .on('end', onGood)
          .on('error', onError)
          .on('finish', onGood)
          .on('filesLimit', onError)

        ctx.req.pipe(busboy)
      })
    } catch (e) {
      console.error(e)
      return response(ctx, { msg: e, data: null }, true, 500)
    }
    console.log(info)

    let item = await ctx.db['datasets'].create(info);
    await validDBModifyOperation(ctx, next, item);
    responseSuccess(ctx, next, item);
    // responseSuccess(ctx, next, {});
  }

  async update(ctx, next) {

    if (bucketExistsIndexDatasets === null) {
      bucketExistsIndexDatasets = await minioClient.bucketExists('dataset')
      if (!bucketExistsIndexDatasets) {
        await minioClient.makeBucket('dataset')
      }
    }

    if (bucketExistsIndexTransformer === null) {
      bucketExistsIndexTransformer = await minioClient.bucketExists('transformer')
      if (!bucketExistsIndexTransformer) {
        await minioClient.makeBucket('transformer')
      }
    }

    let info

    try {

      const lastItem = await ctx.db['datasets'].fetchItemById(ctx.params._id);

      console.log(lastItem)

      info = await new Promise((resolve, reject) => {
        const info = {}

        const busboy = new Busboy({
          headers: ctx.req.headers,
          limits: {
            files: 2
          }
        });

        const fileStreams = []

        const onFile = async (fieldname, file, filename) => {
          try {
            let name = path.basename(filename)
            const ext = path.extname(name)
            name = name.replace(ext, '')
            name = `${name}-${nanoid()}${ext}`
            switch (fieldname) {
              case 'dataset':
                if (bucketExistsIndexDatasets !== null) {
                  info['dataset'] = name
                  fileStreams.push(new Promise(async (res, rej) => {
                    try {
                      if (typeof lastItem.dataset === 'string') {
                        await minioClient.removeObject('dataset', lastItem.dataset)
                      }
                      await minioClient.putObject('dataset', name, file)
                      res()
                    } catch (e) {
                      console.error('Error dataset load to S3')
                      rej(e)
                    }
                  }))
                } else {
                  console.error('dataset bucket don\'t exist')
                  cleanup()
                  reject(new Error('dataset bucket don\'t exist'))
                }
                break
              case 'transformer':
                if (bucketExistsIndexTransformer !== null) {
                  info['transformer'] = name
                  fileStreams.push(new Promise(async (res, rej) => {
                    try {
                      if (typeof lastItem.transformer === 'string') {
                        await minioClient.removeObject('transformer', lastItem.transformer)
                      }
                      await minioClient.putObject('transformer', name, file)
                      res()
                    } catch (e) {
                      console.error('Error transform load to S3')
                      rej(e)
                    }
                  }))
                } else {
                  console.error('transformer bucket don\'t exist')
                  cleanup()
                  reject(new Error('transformer bucket don\'t exist'))
                }
                break
              default:
                cleanup()
                reject(new Error('Undefined field of file'))
                break
            }
          } catch (e) {
            console.error('Unexpected error', e)
            cleanup()
            reject(e)
          }
        }

        const onField = (fieldname, val) => {
          info[fieldname] = val.toString()
        }

        const onGood = async () => {
          console.log(fileStreams)
          console.log('FINISH')
          if (fileStreams.length > 0) {
            await Promise.all(fileStreams)
          }
          cleanup()
          resolve(info)
        }

        const onError = (err) => {
          console.error("ERROR EVENT", err)
          cleanup()
          reject(err)
        }

        const cleanup = () => {
          busboy.removeListener('file', onFile)
          busboy.removeListener('field', onField)
          busboy.removeListener('close', onError)
          busboy.removeListener('end', onGood)
          busboy.removeListener('error', onError)
          busboy.removeListener('finish', onGood)
          busboy.removeListener('filesLimit', onError)
        }

        busboy
          .on('file', onFile)
          .on('field', onField)
          .on('close', onError)
          .on('end', onGood)
          .on('error', onError)
          .on('finish', onGood)
          .on('filesLimit', onError)

        ctx.req.pipe(busboy)
      })
    } catch (e) {
      console.error(e)
      return response(ctx, { msg: e, data: null }, true, 500)
    }

    console.log(info)

    let item = await ctx.db['datasets'].update(
      ctx.params._id,
      info
    );
    await validDBModifyOperation(ctx, next, item);
    responseSuccess(ctx, next, item);
  }

  async delete(ctx, next) {
    const lastItem = await ctx.db['datasets'].fetchItemById(ctx.params._id)
    if (typeof lastItem.dataset === 'string') {
      await minioClient.removeObject('dataset', lastItem.dataset)
    }
    if (typeof lastItem.transformer === 'string') {
      await minioClient.removeObject('transformer', lastItem.transformer)
    }
    console.log(ctx.params._id)
    let item = await ctx.db['datasets'].delete(ctx.params._id)
    await validDBModifyOperation(ctx, next, item)
    responseSuccess(ctx, next, { success: 'ok' })
  }
};