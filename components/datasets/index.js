const Router = require('koa-router');
const Datasets = require('./api');
const { validObjectId, validBody, validShortId, validLang } = require('./../../utils');
const validationSchema = require('./../../schemas/datasets.json');

module.exports = class DatasetsRouter {
  constructor(app, router) {
    this.globalRouter = router;
    this.router = new Router({
      prefix: '/datasets'
    });
    this.app = app;
    this.datasets = new Datasets('datasets');

    this.validateBody = async (ctx, next) => validBody(ctx, next, validationSchema, 'datasets');
    this.router.param('_id',      validObjectId);
    this.router.param('_shortId', validShortId);
    this.router.param('lang',     validLang);

    this.router.get('/', this.datasets.read);
    this.router.get('/:_id', this.datasets.readById);
    // this.validateBody, 
    this.router.post('/', this.validateBody, this.datasets.create);
    this.router.put('/:_id', this.validateBody, this.datasets.update);
    this.router.del('/:_id', this.datasets.delete);

    // this.router.get('/:_shortId', this.xxxxx.readByShortId);
    // this.router.post('/', this.validateBody, this.xxxxx.createMultiLang);
    // this.router.get('/:lang\::_shortId', this.xxxxx.readItemByLang);
    // this.router.get('/lang/:lang', this.xxxxx.readItemsByLang);

    this.globalRouter.use(this.router.routes(), this.router.allowedMethods());
  }
};