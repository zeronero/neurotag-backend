const Router = require('koa-router');
const fs = require('fs');

module.exports = class ApiRoot {
  constructor(app) {
    this.router = new Router({
      prefix: '/api'
    });

    this.app = app;
    this.app.use(this.router.routes());
    this.app.use(this.router.allowedMethods());

    let path = './components';
    let items = fs.readdirSync(path);
    items.forEach(item => {
      let includePath = `${path}/${item}`;
      if (fs.lstatSync(includePath).isDirectory()) {
        if(item === '_template') return;

        try {
          const Mod = require(`./${item}`);
          new Mod(app, this.router);
        } catch(e) {
          console.log('ApiRoot Error:', e);
        }
      }
    });
  }
};
