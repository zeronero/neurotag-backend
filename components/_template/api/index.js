const { validDBModifyOperation, validDBReadByIdOperation, responseSuccess } = require('./../../../utils/');

module.exports = class Zzzzzz {
  constructor() { }
  async readByShortId(ctx, next) {
    let item = await ctx.db['xxxxx'].fetchItemsByShortId(ctx.params._shortId);
    await validDBReadByIdOperation(ctx, next, item);
    responseSuccess(ctx, next, item);
  }

  async readById(ctx, next) {
    let item = await ctx.db['xxxxx'].fetchItemById(ctx.params._id);
    await validDBReadByIdOperation(ctx, next, item);
    responseSuccess(ctx, next, item);
  }

  async readItemByLang(ctx, next) {
    let item = await ctx.db['xxxxx'].fetchItemByLang(ctx.params._shortId, ctx.params.lang);
    await validDBReadByIdOperation(ctx, next, item);
    responseSuccess(ctx, next, item);
    return;
  }

  async readItemsByLang(ctx, next) {
    let item = await ctx.db['xxxxx'].fetchItemsByLang(ctx.params.lang);
    await validDBReadByIdOperation(ctx, next, item);
    responseSuccess(ctx, next, item);
    return;
  }

  // filters =
  // { page: '7',
  //   size: '25',
  //   sortBy: 'null',
  //   sortDesc: 'false',
  //   filter: 'test' }
  async read(ctx, next) {
    let query = {};
    let filters = {};
    if(ctx.query !== undefined) {
      if(ctx.query.page) {
        let page = parseInt(ctx.query.page) - 1;

        filters.page = page;

        if(page <= 0 || page >= 1000 || isNaN(page))
          filters.page = 0;
      }

      if(ctx.query.size) {
        let size = parseInt(ctx.query.size);

        filters.size = size;

        if(size <= 0 || size >= 100 || isNaN(size))
          filters.size = 25;
      }

      if(ctx.query.filter) {
        let filter = (ctx.query.filter).toString().trim();

        filters.filter = filter;

        if(filter.length === 0)
          delete filters.filter;
      }

      if(ctx.query.sortBy) {
        let sortBy = (ctx.query.sortBy).toString().trim();

        filters.sortBy = sortBy;

        if(sortBy.length === 0 || sortBy === 'null')
          delete filters.sortBy;
      }

      if(ctx.query.filter) {
        let filter = (ctx.query.filter).toString().trim();

        filters.filter = filter;

        if(filter.length === 0)
          delete filters.filter;
      }


    }
    let items = await ctx.db['xxxxx'].read(query, filters);

    if(items.total !== undefined)
      responseSuccess(ctx, next, items.items, items.total);
    else
      responseSuccess(ctx, next, items);
  }

  async createMultiLang(ctx, next) {
    let item = await ctx.db['xxxxx'].createMultiLang(ctx.state['xxxxx']);
    responseSuccess(ctx, next, item);
  }

  async create(ctx, next) {
    let item = await ctx.db['xxxxx'].create(ctx.state['xxxxx']);
    await validDBModifyOperation(ctx, next, item);
    responseSuccess(ctx, next, item);
  }

  async update(ctx, next) {
    let item = await ctx.db['xxxxx'].update(
      ctx.params._id,
      ctx.state['xxxxx']
    );
    await validDBModifyOperation(ctx, next, item);
    responseSuccess(ctx, next, item);
  }

  async delete(ctx, next) {
    let item = await ctx.db['xxxxx'].delete(ctx.params._shortId);
    await validDBModifyOperation(ctx, next, item);
    responseSuccess(ctx, next, { success: 'ok' });
  }
};