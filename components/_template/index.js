const Router = require('koa-router');
const Zzzzzz = require('./api');
const { validObjectId, validBody, validShortId, validLang } = require('./../../utils');
const validationSchema = require('./../../schemas/xxxxx.json');

module.exports = class ZzzzzzRouter {
  constructor(app, router) {
    this.globalRouter = router;
    this.router = new Router({
      prefix: '/xxxxx'
    });
    this.app = app;
    this.xxxxx = new Zzzzzz('xxxxx');

    this.validateBody = async (ctx, next) => validBody(ctx, next, validationSchema, 'xxxxx');
    this.router.param('_id',      validObjectId);
    this.router.param('_shortId', validShortId);
    this.router.param('lang',     validLang);

    this.router.get('/',     this.xxxxx.read);
    this.router.get('/:_id', this.xxxxx.readById);
    this.router.post('/',    this.validateBody, this.xxxxx.create);
    this.router.put('/:_id', this.validateBody, this.xxxxx.update);
    this.router.del('/:_id', this.xxxxx.delete);

    // this.router.get('/:_shortId', this.xxxxx.readByShortId);
    // this.router.post('/', this.validateBody, this.xxxxx.createMultiLang);
    // this.router.get('/:lang\::_shortId', this.xxxxx.readItemByLang);
    // this.router.get('/lang/:lang', this.xxxxx.readItemsByLang);

    this.globalRouter.use(this.router.routes(), this.router.allowedMethods());
  }
};